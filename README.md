# Yubico Helper

Tools to configure your Linux system with your Yubikey. PAM and udev are handled at the moment. So you could be login as 2FA with your yubikey or use it to login to your session. Sudo and polkit are also managed if you want to get rid entering your password each time.

## Usage

Here are the help messages about the various helper subcommands. The PAM
configuration files that the tool manage are the following three files :

- sudo : sudo cli authentication mechanism
- polkit-1 : Policy kit used by Windows environment to
- system-auth : System authentication (was that not obvious ?)

The 3 files are processed at once and could be not be managed separately. The
tool inserts a configuration snippet before a certain pattern which are :

- system-auth : `^auth.*pam_unix.so`
- sudo : `^auth.*pam_env.so`
- polkit-1 : `^auth.*pam_env.so`

```bash
$ yubico_helper -h
Configure your system to use a Yubikey using either pam_yubico or pam_u2f module.

Usage: yubico_helper [OPTIONS] <COMMAND>

Commands:
  yubico  Configure pam_yubico for your system
  u2f     Configure pam_u2f for your system
  udev    Use UDev to automatically lock your session when you unplugged your key Display the status without arguments provided
  help    Print this message or the help of the given subcommand(s)

Options:
  -c, --cleanup
          Cleanup yubico_helper configuration snippets from files
  -d, --dryrun
          Dry run the modification and displaying them in the stdout
  -v, --verbose
          Increase verbosity
      --pam-systemauth-file <PAM_SYSTEMAUTH_FILE>
          Specify where is the system-auth pam file [default: /etc/pam.d/system-auth]
      --pam-polkit-file <PAM_POLKIT_FILE>
          Specify where is the polkit-1 pam file [default: /etc/pam.d/polkit-1]
      --pam-sudo-file <PAM_SUDO_FILE>
          Specify where is the sudo pam file [default: /etc/pam.d/sudo]
  -h, --help
          Print help
  -V, --version
          Print version
```

```bash
$ yubico_helper u2f -h
Configure pam_u2f for your system

Usage: yubico_helper u2f [OPTIONS] <AUTH>

Arguments:
  <AUTH>  How do you prefer to use pam_yubico [possible values: sufficient, required]

Options:
  -d, --debug    Enable or disable the debug flag
  -f, --force    Force to write no matter if an existing line using U2F PAM modules exists
  -h, --help     Print help (see more with '--help')
  -V, --version  Print version
```

```bash
$ yubico_helper yubico -h
Configure pam_yubico for your system

Usage: yubico_helper yubico [OPTIONS] --id <ID> <AUTH> <AUTHMODE>

Arguments:
  <AUTH>      How do you prefer to use pam_yubico [possible values: sufficient, required]
  <AUTHMODE>  Which mode you want to use with pam_yubico [possible values: client, challenge-response]

Options:
  -d, --debug    Enable or disable the debug flag
  -i, --id <ID>  Your API Client ID inf the Yubico validation server. Go here to use the default YubiCloud service: https://upgrade.yubico.com/getapikey
  -f, --force    Force to write no matter if an existing line using Yubico PAM module exists
  -h, --help     Print help (see more with '--help')
  -V, --version  Print version
```

```bash
$ yubico_helper udev -h
Use UDev to automatically lock your session when you unplugged your key Display the status without arguments provided

Usage: yubico_helper udev [OPTIONS]

Options:
  -e, --enabled  Add an Udev rules to automatically lock your session when the usb key is unplugged
  -d, --disabled  Comment the rule that autolock your session when the yubikey is unplugged
  -h, --help     Print help
  -V, --version  Print version
```

## Usage/Examples

Dry-run configuration of your system to login with the yubico and the Universal
2 factor PAM module :

```bash
$ sudo yubico_helper -c -d u2f sufficient
```

Display whether or not the udev rule to autolock your session is set :

```bash
$ sudo yubico_helper udev
Udev rule is not set.
```

Enable the session autolock when the usb yubikey is unplugged :

```bash
$ sudo yubico_helper udev --enabled
```

## Authors

- [@claneys](https://gitlab.com/claneys)
