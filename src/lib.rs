use clap::ValueEnum;
use core::panic;
use libusb::Context;
use passwd_rs::User;
use regex::Regex;
use std::error::Error;
use std::fs;
use std::io::Write;
use std::os::unix::fs::MetadataExt;
use std::path::{Path, PathBuf};
extern crate libc;
use libc::{c_char, chown};
use std::ffi::CString;

const BEGIN_BOUNDARY: &str = "##### BEGIN YUBICO HELPER #####";
const END_BOUNDARY: &str = "###### END YUBICO HELPER ######";
const YUBICO_VENDOR_ID: u16 = 0x1050;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum Auth {
    /// Use as a password-less authentication method
    Sufficient,
    /// Use as a Second factor authentication method
    Required,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum AuthMode {
    /// Online validation with a YubiKey validation service such as the YubiCloud
    Client,
    /// Offline validation using YubiKeys with HMAC-SHA-1 Challenge-Response configurations
    ChallengeResponse,
}

pub fn pamconfiguration_line(
    module: String,
    auth: Auth,
    authmode: Option<AuthMode>,
    id: Option<String>,
    debug: bool,
) -> String {
    let id = id.unwrap_or(String::from("YUBIKEY ID"));
    let authmode = authmode.unwrap_or(AuthMode::Client);

    let ret = match module.as_str() {
        "yubico" => {
            let ret = match auth {
                Auth::Sufficient => {
                    format!("auth sufficient pam_yubico.so id={}", id)
                }
                Auth::Required => {
                    format!("auth required pam_yubico.so id={}", id)
                }
            };
            if authmode == AuthMode::ChallengeResponse {
                format!("{} mode=challenge-response chalresp_path=/var/yubico", ret)
            } else {
                ret
            }
        }
        "u2f" => match auth {
            Auth::Sufficient => String::from("auth sufficient pam_u2f.so cue"),
            Auth::Required => String::from("auth required pam_u2f.so cue"),
        },
        _ => panic!("No module specified which should not occurs..."),
    };

    if debug {
        ret + " debug"
    } else {
        ret
    }
}

pub fn pamstatus(content: &String) -> Result<(), String> {
    let mut lineblock_flag = false;
    let lines = content.lines();
    let mut line_nb = 0;

    for line in lines {
        line_nb += 1;
        if !lineblock_flag && line.contains(BEGIN_BOUNDARY) {
            lineblock_flag = true;
            continue;
        } else if lineblock_flag && line.contains(END_BOUNDARY) {
            lineblock_flag = false;
            continue;
        } else if lineblock_flag {
            continue;
        };
        if !line.starts_with("#") && line.contains("pam_u2f.so") {
            return Err(format!(
                "Line '{}' uses pam_u2f, please comment it out or remove it before proceding.",
                line_nb
            ));
        } else if !line.starts_with("#") && line.contains("pam_yubico.so") {
            return Err(format!(
                "Line '{}' uses pam_yubico, please comment it out or remove it before proceding.",
                line_nb
            ));
        }
    }
    Ok(())
}

pub fn modify_pamfiles(
    conf_files: [PathBuf; 3],
    line: String,
    force: bool,
    verbose: bool,
    dryrun: bool,
    action: fn(&str, &String, &String, bool, bool) -> std::string::String,
) {
    let before_pattern = [
        r"^auth.*pam_unix.so",
        r"^auth.*pam_env.so",
        r"^auth.*pam_env.so",
    ];

    for i in 0..3 {
        if let Err(e) = modify_conffile(
            before_pattern[i],
            &conf_files[i],
            &line,
            &User::new_from_uid(0).unwrap(),
            force,
            verbose,
            dryrun,
            action,
        ) {
            println!("Error on write '{}': {}", conf_files[i].display(), e);
            std::process::exit(1);
        };
    }
}

pub fn udev_status(udev_file: PathBuf, pattern: &String) -> Result<String, String> {
    if Path::new(&udev_file).exists() {
        let content: String = fs::read_to_string(&udev_file)
            .unwrap_or_else(|s| format!("Udev file with error: {}.", s));
        for line in content.lines() {
            if line == pattern {
                return Ok(String::from(
                    "Udev rule is set. Your system will lock when you unplug your yubikey.\n",
                ));
            }
        }
    }
    return Err(String::from("Yubico Udev rule is not set.\n"));
}

fn add_line(before: &str, line2add: &String, content: &String) -> String {
    let mut output: String = String::new();
    let pattern = Regex::new(before).unwrap();
    let snippet = format!("{}\n{}\n{}\n", BEGIN_BOUNDARY, line2add, END_BOUNDARY);

    if before.len() == 0 {
        output.push_str(snippet.as_str());
        output.push_str(content.trim_end());
    } else {
        for line in content.lines() {
            if pattern.is_match(line) {
                output.push_str(snippet.as_str());
            }
            output.push_str(format!("{}\n", line).as_str());
        }
    }
    output
}

pub fn remove_confline(
    _before: &str,
    content: &String,
    _yubikey_line: &String,
    _force: bool,
    verbose: bool,
) -> String {
    let mut output: String = String::new();
    let lines = content.lines();
    let mut lineblock_flag = false;
    for line in lines {
        if !lineblock_flag && line.contains(BEGIN_BOUNDARY) {
            lineblock_flag = true;
            continue;
        } else if lineblock_flag && line.contains(END_BOUNDARY) {
            lineblock_flag = false;
            continue;
        } else if lineblock_flag {
            continue;
        };
        output.push_str(format!("{}\n", line).as_str());
    }

    if verbose {
        println!("After cleaned up the file: {}", output);
    }

    output
}

pub fn insert_confline(
    before: &str,
    content: &String,
    yubikey_line: &String,
    force: bool,
    verbose: bool,
) -> String {
    let mut content = remove_confline(before, content, yubikey_line, force, verbose);

    content = add_line(before, &yubikey_line, &content);
    if verbose {
        println!("After added the line file: {}\n", content);
    }

    content
}

fn change_owner(fpath: &Path, uid: u32, gid: u32) -> Result<(), String> {
    if let Ok(meta) = fs::metadata(&fpath) {
        if meta.uid() != uid {
            let cpath = CString::new(fpath.to_str().unwrap()).map_err(|e| e.to_string())?;
            let result = unsafe { chown(cpath.as_ptr() as *const c_char, uid, gid) };

            if result == 0 {
                return Ok(());
            } else {
                return Err(format!("Failed to change file owner: {}", result));
            }
        }
        return Ok(());
    }

    Err(format!(
        "Something went wrong reading metadata of {}",
        fpath.display()
    ))
}

pub fn modify_conffile(
    before: &str,
    conffile: &PathBuf,
    yubikey_line: &String,
    owner: &User,
    force: bool,
    verbose: bool,
    dryrun: bool,
    action: fn(&str, &String, &String, bool, bool) -> String,
) -> Result<(), Box<dyn Error>> {
    let dir = conffile.parent().unwrap_or_else(|| Path::new(""));
    if dir.try_exists().is_ok_and(|result| !result) {
        match fs::create_dir_all(dir) {
            Ok(()) => println!("{} created successfully.", dir.display()),
            Err(err) => println!("Error creating directory: {}", err),
        }
        change_owner(dir, owner.uid, owner.gid)?;
    }

    let output = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .open(&conffile)?;
    let mut content: String = fs::read_to_string(&conffile)?;
    if !force {
        pamstatus(&content)?;
    }
    content = action(before, &content, yubikey_line, force, verbose);

    if dryrun {
        println!("File {}:\n {}", conffile.display(), content);
    } else {
        write!(&output, "{}", content)?;
    }

    change_owner(conffile, owner.uid, owner.gid)?;
    Ok(())
}

pub fn get_usb_product_id() -> Result<u16, String> {
    let usb = Context::new().unwrap();

    let devices = usb.devices().unwrap();
    for device in devices.iter() {
        let vendor = device.device_descriptor().unwrap().vendor_id();
        if vendor == YUBICO_VENDOR_ID {
            return Ok(device.device_descriptor().unwrap().product_id());
        }
    }

    return Err("Did not find any USB devices matching the Yubico vendor ID. Are you sure having inserted the Yubikey ?".to_string());
}

#[cfg(test)]
mod tests {
    use crate::{pamconfiguration_line, AuthMode};

    #[test]
    fn check_u2f_pamline_sufficient() {
        assert_eq!(
            "auth sufficient pam_u2f.so cue",
            pamconfiguration_line(
                String::from("u2f"),
                crate::Auth::Sufficient,
                None,
                None,
                false,
            )
        );
    }

    #[test]
    fn check_u2f_pamline_sufficient_debug() {
        assert_eq!(
            "auth sufficient pam_u2f.so cue debug",
            pamconfiguration_line(
                String::from("u2f"),
                crate::Auth::Sufficient,
                None,
                None,
                true,
            )
        );
    }

    #[test]
    fn check_u2f_pamline_required() {
        assert_eq!(
            "auth required pam_u2f.so cue",
            pamconfiguration_line(
                String::from("u2f"),
                crate::Auth::Required,
                None,
                None,
                false,
            )
        );
    }

    #[test]
    fn check_u2f_pamline_required_debug() {
        assert_eq!(
            "auth required pam_u2f.so cue debug",
            pamconfiguration_line(String::from("u2f"), crate::Auth::Required, None, None, true,)
        );
    }

    #[test]
    fn check_yubico_pamline() {
        assert_eq!(
            "auth sufficient pam_yubico.so id=1234",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Sufficient,
                Some(AuthMode::Client),
                Some(String::from("1234")),
                false,
            )
        );
    }

    #[test]
    fn check_yubico_pamline_required() {
        assert_eq!(
            "auth required pam_yubico.so id=1234",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Required,
                Some(AuthMode::Client),
                Some(String::from("1234")),
                false,
            )
        );
    }

    #[test]
    fn check_yubico_pamline_sufficient_challenge_response() {
        assert_eq!(
            "auth sufficient pam_yubico.so id=1234 mode=challenge-response chalresp_path=/var/yubico",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Sufficient,
                Some(AuthMode::ChallengeResponse),
                Some(String::from("1234")),
                false,
            )
        );
    }

    #[test]
    fn check_yubico_pamline_required_challenge_response() {
        assert_eq!(
            "auth required pam_yubico.so id=1234 mode=challenge-response chalresp_path=/var/yubico",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Required,
                Some(AuthMode::ChallengeResponse),
                Some(String::from("1234")),
                false,
            )
        );
    }

    #[test]
    fn check_yubico_pamline_sufficient_debug() {
        assert_eq!(
            "auth sufficient pam_yubico.so id=1234 debug",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Sufficient,
                Some(AuthMode::Client),
                Some(String::from("1234")),
                true,
            )
        );
    }

    #[test]
    fn check_yubico_pamline_required_challenge_response_debug() {
        assert_eq!(
            "auth required pam_yubico.so id=1234 mode=challenge-response chalresp_path=/var/yubico debug",
            pamconfiguration_line(
                String::from("yubico"),
                crate::Auth::Required,
                Some(AuthMode::ChallengeResponse),
                Some(String::from("1234")),
                true,
            )
        );
    }
}
