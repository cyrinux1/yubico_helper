use clap::{Parser, Subcommand};
use passwd_rs::User;
use std::path::PathBuf;
use std::process::Command;
use std::{env, process};
use yubico_helper::{
    get_usb_product_id, insert_confline, modify_conffile, modify_pamfiles, pamconfiguration_line,
    remove_confline, udev_status, Auth, AuthMode,
};

const UDEV_FILE: &str = "/etc/udev/rules.d/20-yubikey.rules";
const PAM_SYSTEM_AUTH_FILE: &str = "/etc/pam.d/system-auth";
const PAM_SYSTEM_POLKIT: &str = "/etc/pam.d/polkit-1";
const PAM_SYSTEM_SUDO: &str = "/etc/pam.d/sudo";

#[derive(Parser)]
#[clap(name = "yubico_helper", version = "0.1.0", author = "Romain Forlot")]
#[clap(
    about = "Configure your system to use a Yubikey using either pam_yubico or pam_u2f module.",
    propagate_version = true
)]
struct YubicoHelper {
    #[clap(subcommand)]
    command: Commands,

    /// Cleanup yubico_helper configuration snippets from files
    #[clap(long, short = 'c', default_value_t = false)]
    cleanup: bool,
    /// Dry run the modification and displaying them in the stdout
    #[clap(long, short = 'd', default_value_t = false)]
    dryrun: bool,
    /// Increase verbosity
    #[clap(long, short = 'v', default_value_t = false)]
    verbose: bool,
    /// Specify where is the system-auth pam file
    #[clap(long, default_value_t = String::from(PAM_SYSTEM_AUTH_FILE))]
    pam_systemauth_file: String,
    /// Specify where is the polkit-1 pam file
    #[clap(long, default_value_t = String::from(PAM_SYSTEM_POLKIT))]
    pam_polkit_file: String,
    /// Specify where is the sudo pam file
    #[clap(long, default_value_t = String::from(PAM_SYSTEM_SUDO))]
    pam_sudo_file: String,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Configure pam_yubico for your system
    Yubico {
        /// How do you prefer to use pam_yubico :
        #[arg(value_enum)]
        auth: Auth,
        /// Which mode you want to use with pam_yubico
        #[arg(value_enum)]
        authmode: AuthMode,
        /// Enable or disable the debug flag
        #[arg(short = 'd', long)]
        debug: bool,
        /// Your API Client ID inf the Yubico validation server. Go here to use
        /// the default YubiCloud service: https://upgrade.yubico.com/getapikey
        #[arg(short = 'i', long)]
        id: String,
        /// Force to write no matter if an existing line using Yubico PAM
        /// module exists.
        #[arg(short = 'f', long)]
        force: bool,
    },
    /// Configure pam_u2f for your system
    U2F {
        /// How do you prefer to use pam_u2f :

        #[arg(value_enum)]
        auth: Auth,
        /// Enable or disable the debug flag
        #[arg(short = 'd', long)]
        debug: bool,
        /// Force to write no matter if an existing line using U2F PAM
        /// modules exists.
        #[arg(short = 'f', long)]
        force: bool,
    },
    /// Use UDev to automatically lock your session when you unplugged your key
    /// Display the status without arguments provided.
    Udev {
        /// Change the default udev rules file name
        #[arg(short = 'f', long, default_value_t = String::from(UDEV_FILE))]
        filename: String,
        /// Add an Udev rules to automatically lock your session when
        /// the usb key is unplugged
        #[arg(short = 'e', long, default_value_t = false)]
        enabled: bool,
        /// Comment the rule that autolock your session when the yubikey is
        /// unplugged
        #[arg(short = 'd', long, default_value_t = false)]
        disabled: bool,
        #[arg(short = 'p', long, hide = true, required = false, default_value_t = 0)]
        product_id: u16,
    },
}

fn main() {
    let args = YubicoHelper::parse();
    let systemauth: PathBuf = PathBuf::from(args.pam_systemauth_file);
    let sudo: PathBuf = PathBuf::from(args.pam_sudo_file);
    let polkit: PathBuf = PathBuf::from(args.pam_polkit_file);
    let verbose = args.verbose;
    let dryrun = args.dryrun;
    let user;
    let conf_files = [systemauth, sudo, polkit];

    let action = if args.cleanup {
        remove_confline
    } else {
        insert_confline
    };

    if env::var("SUDO_USER").is_ok() {
        user = User::new_from_name(&env::var("SUDO_USER").unwrap()).unwrap();
    } else {
        user = User::new_from_name(&env::var("USERNAME").unwrap_or_else(|_e| {
            env::var("USER").unwrap_or_else(|e| {
                println!("Error: {}", e);
                process::exit(1);
            })
        }))
        .expect("Error: User not found in your system.");
    }

    match args.command {
        Commands::U2F { auth, debug, force } => {
            let output = Command::new("pamu2fcfg").args([format!("-u {}", user.name)]).output().unwrap_or_else(|e| {
                println!("Error trying to execute 'pamu2fcfg' tool: {}\n You need to install it along with the PAM module to use your Yubikey with u2f.\n You can find it here: https://developers.yubico.com/pam-u2f/", e);
                process::exit(1);
            });
            let u2f_keys_file: PathBuf =
                PathBuf::from(format!("{}/.config/Yubico/u2f_keys", user.homedir));
            if !dryrun {
                modify_conffile(
                    "",
                    &u2f_keys_file,
                    &String::from_utf8(output.stdout)
                        .unwrap()
                        .strip_prefix(" ")
                        .unwrap()
                        .to_string(),
                    &user,
                    force,
                    verbose,
                    dryrun,
                    action,
                )
                .unwrap_or_else(|e| {
                    println!("Error: {}", e);
                    process::exit(2)
                });
            }
            let line = pamconfiguration_line(String::from("u2f"), auth, None, None, debug);
            modify_pamfiles(conf_files, line, force, verbose, dryrun, action);
        }
        Commands::Yubico {
            auth,
            authmode,
            debug,
            id,
            force,
        } => {
            let line = pamconfiguration_line(
                String::from("yubico"),
                auth,
                Some(authmode),
                Some(id),
                debug,
            );
            modify_pamfiles(conf_files, line, force, verbose, dryrun, action);
        }
        Commands::Udev {
            filename,
            enabled,
            disabled,
            product_id,
        } => {
            let usb_product_id = get_usb_product_id().unwrap_or_else(|_e| product_id);
            let mut line = format!("ACTION==\"remove\", ENV{{ID_BUS}}==\"usb\", ENV{{ID_MODEL_ID}}==\"{:04x}\", ENV{{ID_VENDOR_ID}}==\"1050\", RUN+=\"/bin/loginctl lock-sessions\"", usb_product_id);

            let status = udev_status(PathBuf::from(&filename), &line);
            match status {
                Ok(msg) => {
                    if enabled || !disabled {
                        print!("{}", msg);
                        return;
                    } else if disabled {
                        line.insert(0, '#');
                    }
                }
                Err(msg) => {
                    if disabled || !enabled {
                        print!("{}", msg);
                        return;
                    }
                }
            }

            if let Err(e) = modify_conffile(
                "",
                &PathBuf::from(&filename),
                &line,
                &user,
                false,
                args.verbose,
                args.dryrun,
                action,
            ) {
                println!("Error on write '{}': {}", filename, e);
                process::exit(1);
            }
        }
    };
}
