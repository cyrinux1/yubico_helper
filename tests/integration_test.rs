use std::process::Command;
extern crate yubico_helper;

#[test]
fn udev_status_nofile() {
    let output = Command::new("./target/debug/yubico_helper")
        .args(["udev", "-p", "1031", "-f", "tests/no_such_file.rules"])
        .output()
        .unwrap();
    assert_eq!(
        "\"Yubico Udev rule is not set.\\n\"",
        format!("{:?}", String::from_utf8(output.stdout).unwrap())
    );
}

#[test]
fn udev_status_enabled() {
    let output = Command::new("./target/debug/yubico_helper")
        .args(["udev", "-p", "1031", "-f", "tests/enabled_udev.rules"])
        .output()
        .unwrap();
    assert_eq!(
        "\"Udev rule is set. Your system will lock when you unplug your yubikey.\\n\"",
        format!("{:?}", String::from_utf8(output.stdout).unwrap())
    );
}

#[test]
fn udev_status_disabled() {
    let output = Command::new("./target/debug/yubico_helper")
        .args(["udev", "-p", "1031", "-f", "tests/disabled_udev.rules"])
        .output()
        .unwrap();
    assert_eq!(
        "\"Yubico Udev rule is not set.\\n\"",
        format!("{:?}", String::from_utf8(output.stdout).unwrap())
    );
}
